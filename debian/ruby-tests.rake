require 'rake/testtask'
Rake::TestTask.new(:pure_ruby) do |t|
  t.libs << 'test'
  t.test_files = FileList['test/test_pure_ruby.rb']
  t.verbose = true
end
task :default => :pure_ruby
